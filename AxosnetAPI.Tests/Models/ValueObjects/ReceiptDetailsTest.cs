﻿using System;
using AxosnetAPI.Models.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AxosnetAPI.Tests.Models.ValueObjects
{
    [TestClass]
    public class ReceiptDetailsTest
    {
        [TestMethod]
        public void AmountLessZero_Exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ReceiptDetails(-1, "MXM", "Prueba de recibo"));
        }

        [TestMethod]
        public void CurrencyNullOrEmpty_Exception()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ReceiptDetails(1, "", "Prueba de recibo"));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ReceiptDetails(1, null, "Prueba de recibo"));
        }

        [TestMethod]
        public void ReceiptDetailsOk()
        {
            try
            {
                var mexicana = new ReceiptDetails(100, "MXM", "Prueba de moneda mexicana");
                var dolar = new ReceiptDetails(100, "DOLAR", "Prueba de moneda americana");
            }
            catch (Exception ex)
            {
                Assert.Fail("Fallo la construccion del objeto");
            }
        }
    }
}
