﻿using AxosnetAPI.Entities;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AxosnetAPI.Service
{
    public class TokenService
    {

        /// <summary>
        /// Genera un token con los datos embebidos importantes para
        /// reconocer de quien es la sesion
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public string GenerateToken(Account account)
        {
            // Obtenemos valores para crear el token
            var secretKey = ConfigurationManager.AppSettings[TokenValidationHandler.secretSettingkey];
            var audienceToken = ConfigurationManager.AppSettings[TokenValidationHandler.audienceSettingKey];
            var issuerToken = ConfigurationManager.AppSettings[TokenValidationHandler.issuerSettingKey];
            var expirationTime = ConfigurationManager.AppSettings[TokenValidationHandler.expireSettingKey];
            // Otenemos los bytes de la clave de seguirdad
            var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));
            // Creamos credenciales
            var signingCredetials = new SigningCredentials(securityKey,SecurityAlgorithms.HmacSha256);
            // Ponemos las claims
            ClaimsIdentity claimsIdentity = new ClaimsIdentity();
            List<Claim> claimsToken = new List<Claim>();

            claimsIdentity.AddClaims(claimsToken);
            claimsIdentity.AddClaims(new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Email, account.Access.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("Id", account.AccountID.ToString())
            });

            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();

            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken
                (
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expirationTime)),
                signingCredentials: signingCredetials
                );

            var jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);
            return jwtTokenString;
        }

        public static bool HasValidLifeTime(string token)
        {
            return true;
        }

        
    }
}