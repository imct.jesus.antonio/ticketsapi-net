﻿using AxosnetAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace AxosnetAPI.Service
{
    public class Result
    {
        private bool State { get; set; }
        private string Message { get; set; }
        private string Exception { get; set; }
        private List<object> List { get; set; }
        private object Unique { get; set; }
        private HttpStatusCode? HttpStatus { get; set; }

        public Result(Exception ex)
        {
            this.State = false;
            this.Message = "Ha ocurrido un error";
            this.Exception = $"Excepcion: {ex.Message}";
            this.List = null;
            this.Unique = null;
            this.HttpStatus = null;
        }

        /// <summary>
        /// Llena un response pero con listas
        /// </summary>
        /// <param name="message"></param>
        /// <param name="multi"></param>
        public Result(string message, List<object> multi)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = multi;
            this.Unique = null;
            this.HttpStatus = null;
        }

        /// <summary>
        /// Llena un response pero con un unico objeto
        /// </summary>
        /// <param name="message"></param>
        /// <param name="single"></param>
        public Result(string message, object single)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = null;
            this.Unique = single;
            this.HttpStatus = null;
        }

        /// <summary>
        /// Llena un response con mensajes y estado solamente
        /// </summary>
        /// <param name="message"></param>
        public Result(string message)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = null;
            this.Unique = null;
            this.HttpStatus = null;
        }

        /// <summary>
        /// Llena una respuesta http para servicios rest con cuerpo
        /// </summary>
        /// <param name="message"></param>
        /// <param name="single"></param>
        /// <param name="code"></param>
        public Result(string message, object single, HttpStatusCode code)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = null;
            this.Unique = single;
            this.HttpStatus = code;
        }

        /// <summary>
        /// Llena una respuesta http para servicios rest con cuerpo
        /// </summary>
        /// <param name="message"></param>
        /// <param name="single"></param>
        /// <param name="code"></param>
        public Result(string message, List<object> multi, HttpStatusCode code)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = multi;
            this.Unique = null;
            this.HttpStatus = code;
        }

        /// <summary>
        /// llena una respuesta http para servicios rest sin cuerpo
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public Result(string message, HttpStatusCode code)
        {
            this.State = true;
            this.Message = message;
            this.Exception = null;
            this.List = null;
            this.Unique = null;
            this.HttpStatus = code;
        }

        /// <summary>
        /// Llena una excepcion http para servicios rest
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="code"></param>
        public Result(Exception ex, HttpStatusCode code)
        {
            this.State = false;
            this.Message = "Ha ocurrido un error";
            this.Exception = $"Excepcion: {ex.Message}";
            this.List = null;
            this.Unique = null;
            this.HttpStatus = code;
        }

        /// <summary>
        /// Llena una excepcion http para servicios rest con un
        /// string especifico
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="code"></param>
        //public Result(string error, HttpStatusCode code)
        //{
        //    this.State = false;
        //    this.Message = "Ha ocurrido un error";
        //    this.Exception = error;
        //    this.List = null;
        //    this.Unique = null;
        //    this.HttpStatus = code;
        //}

        public HttpResponseMessage CreateHttpResponse(HttpRequestMessage request)
        {
            this.HttpStatus = HttpStatus.HasValue ? HttpStatus.Value : HttpStatusCode.OK;
            if (Exception is null)
            {
                return request.CreateResponse(HttpStatus.Value,new
                {
                    this.State,
                    this.Message,
                    this.List,
                    this.Unique
                });
            }
            else
            {
                return request.CreateErrorResponse(HttpStatus.Value,Exception);
            }
        }
    }



    public class Response : IResponse
    {
        public bool State { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public int StatusCode { get; set; }
    }

    public class SingleResponse<TModel> : ISingleResponse<TModel>
    {
        public bool State { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public int StatusCode { get; set; }
        public TModel Model { get; set; }
    }

    public class ListResponse<TModel> : IListResponse<TModel>
    {
        public bool State { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public int StatusCode { get; set; }
        public IEnumerable<TModel> Model { get; set; }
    }


}