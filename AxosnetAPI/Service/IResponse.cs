﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxosnetAPI.Models.Interfaces
{

    /// <summary>
    /// Interface que implementa la base que todas las respuestas deben de llevar
    /// como minimo
    /// </summary>
    public interface IResponse
    {
        bool State { get; set; }
        string Message { get; set; }
        string Error { get; set; }
        [JsonIgnore]
        int StatusCode { get; set; }
    }

    /// <summary>
    /// Interface que se implementa para respuestas de
    /// un modelo unico
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface ISingleResponse<TModel> : IResponse
    {
        TModel Model { get; set; }
    }

    /// <summary>
    /// Interface que se implementa para respuestas de un 
    /// modelo con multiples datos una lista
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface IListResponse<TModel> : IResponse
    {
        IEnumerable<TModel> Model { get; set; }
    }
}