﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace AxosnetAPI.Service
{
    public class TokenValidationHandler : DelegatingHandler
    {
        /// <summary>
        /// Variables para crear el token
        /// </summary>
        public static string secretSettingkey = "JWT_SECRET_KEY";
        public static string audienceSettingKey = "JWT_AUDIENCE_TOKEN";
        public static string issuerSettingKey = "JWT_ISSUER_TOKEN";
        public static string expireSettingKey = "JWT_EXPIRE_MINUTES";
        public static string inactivityTimeKey = "JWT_INACTIVITYTIME";

        /// <summary>
        /// Intercepta la peticion, revisa si existe el token y verifica su validez
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpStatusCode statusCode;
            string bearerToken = getBearerAuthorizationToken(request);

            // Revisar si existe o no el token
            if (string.IsNullOrEmpty(bearerToken))
            {
                statusCode = HttpStatusCode.Unauthorized;
                return base.SendAsync(request,cancellationToken);
            }

            try
            {
                TokenValidationParameters tokenValidationParameters = getValidationParameterForAxos();
                SecurityToken securityToken;
                var tokenHandler = new JwtSecurityTokenHandler();
                Thread.CurrentPrincipal = tokenHandler.ValidateToken(bearerToken, tokenValidationParameters, out securityToken);
                HttpContext.Current.User = tokenHandler.ValidateToken(bearerToken, tokenValidationParameters, out securityToken);
                return base.SendAsync(request,cancellationToken);
            }
            catch (SecurityTokenException ex)
            {
                statusCode = HttpStatusCode.Unauthorized;
            }
            catch (Exception ex)
            {
                statusCode = HttpStatusCode.InternalServerError;
            }
            return Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage(statusCode) { });
        }

        /// <summary>
        /// Obtiene los parametros de validacion desde la configuracion
        /// </summary>
        /// <returns></returns>
        private TokenValidationParameters getValidationParameterForAxos()
        {
            var secretKey = ConfigurationManager.AppSettings[secretSettingkey];
            var audienceToken = ConfigurationManager.AppSettings[audienceSettingKey];
            var issuerToken = ConfigurationManager.AppSettings[issuerSettingKey];
            var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));

            return new TokenValidationParameters
            {
                ValidAudience = audienceToken,
                ValidIssuer = issuerToken,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                LifetimeValidator = this.lifeTimeValidator,
                IssuerSigningKey = securityKey
            };
        }

        /// <summary>
        /// Valida que el tiempo de vida del token no haya sido superado
        /// </summary>
        /// <param name="notBefore"></param>
        /// <param name="expires"></param>
        /// <param name="securityToken"></param>
        /// <param name="validationParameters"></param>
        /// <returns></returns>
        private bool lifeTimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (expires != null)
            {
                if (DateTime.UtcNow < expires) { return true; }
            }
            return false;
        }

        /// <summary>
        /// Obtiene el token desde la respuesta que se pasa por paraetro
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private string getBearerAuthorizationToken(HttpRequestMessage request)
        {
            string bearerToken = string.Empty;
            try
            {
                // Obtenemos el token de authorization
                var authHeadersMatch = request.Headers.GetValues("Authorization");
                if (authHeadersMatch == null || authHeadersMatch.Count() != 1)
                {
                    return null;
                }
                var authorizationHeaderValue = authHeadersMatch.First();
                bearerToken = authorizationHeaderValue.StartsWith("Bearer ") ? authorizationHeaderValue.Substring(7) : bearerToken;
                
            }
            catch{ return null; }
            return bearerToken;
        }
    }
}