﻿using AxosnetAPI.Service;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AxosnetAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            // Habilitando cors
            config.EnableCors(new EnableCorsAttribute(origins: "*",headers: "*", methods: "*"));
            // Configurando el formateador
            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatters = formatters.JsonFormatter;
            var settings = jsonFormatters.SerializerSettings;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver(); ;
            // Ignora los miembros nulos
            settings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
            // Removemos la respuesta XML
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            // Añadiendo el handler que manjea los tokens en las peticiones
            config.MessageHandlers.Add(new TokenValidationHandler());
            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
