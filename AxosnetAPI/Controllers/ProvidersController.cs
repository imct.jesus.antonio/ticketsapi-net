﻿using AxosnetAPI.Actions;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AxosnetAPI.Controllers
{
    public class ProvidersController : ApiController
    {
        /// <summary>
        /// Servicio para acceder al proveedor
        /// </summary>
        private readonly ProviderAction _providerAction;

        public ProvidersController()
        {
            _providerAction = new ProviderAction();
        }

        // GET: api/Providers
        /// <summary>
        /// Obtiene todos los proveedores disponibles
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            var status = _providerAction.ProviderLoadAll();
            return status.CreateHttpResponse(Request); ;
        }

        // POST: api/Providers
        /// <summary>
        /// Crea un proveedor con la informacion que existe en el parametro
        /// </summary>
        /// <param name="newProvider"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]Dictionary<string, object> newProvider)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); }
            var status = _providerAction.AddNewProvider(newProvider);
            return status.CreateHttpResponse(Request);
        }

    }
}
