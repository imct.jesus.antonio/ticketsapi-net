﻿using AxosnetAPI.Actions;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AxosnetAPI.Controllers
{
    public class ReceiptsController : ApiController
    {
        /// <summary>
        /// Clase para acceder a las acciones de los receipts
        /// </summary>
        private readonly ReceiptAction _receiptAction;

        /// <summary>
        /// Construcotr public del controller
        /// </summary>
        public ReceiptsController()
        {
            _receiptAction = new ReceiptAction();
        }

        // GET: api/Receipts
        /// <summary>
        /// Carga todos los recibos
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get([FromUri]int providerId,[FromUri]string currency)
        {
            var status = _receiptAction.ReceiptLoadAll(providerId, currency);
            return status.CreateHttpResponse(Request);
        }

        // GET: api/Receipts/5
        /// <summary>
        /// Obtiene un recibo especifico 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(int id)
        {
            var status = _receiptAction.ReceiptSelect(id);
            return status.CreateHttpResponse(Request);
        }

        // POST: api/Receipts
        /// <summary>
        /// Crea un nuevo recibo con los datos que se introducen por body
        /// </summary>
        /// <param name="ReceiptToCreate"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public HttpResponseMessage Post([FromBody]Dictionary<string,object> ReceiptToCreate)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ModelState); }
            var status = _receiptAction.AddNewReceipt(ReceiptToCreate);
            return status.CreateHttpResponse(Request);
        }

        // PUT: api/Receipts/5
        /// <summary>
        /// Actualiza una serie de campos de un recibo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="DataToUpdate"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(int id, [FromBody]Dictionary<string, object> DataToUpdate)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); }
            var status = _receiptAction.UpdateDeferredDetail(id, DataToUpdate);
            return status.CreateHttpResponse(Request);
        }

        // DELETE: api/Receipts/5
        /// <summary>
        /// Toma el id y lo marca como eliminado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete(int id)
        {
            var status = _receiptAction.LogicalDeletionOfReceipt(id);
            return status.CreateHttpResponse(Request);
        }
    }
}
