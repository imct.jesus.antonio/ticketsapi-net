﻿using AxosnetAPI.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AxosnetAPI.Controllers
{
    public class SessionsController : ApiController
    {
        /// <summary>
        /// Caso de uso para la sesion de dominio
        /// </summary>
        public readonly SesionAction _sesionAction;

        /// <summary>
        /// Constructor publico preparado para DI
        /// </summary>
        public SessionsController()
        {
            _sesionAction = new SesionAction();
        }

        // POST: api/Sessions
        /// <summary>
        /// Creamos una sesion cuando iniciamos sesion con las credenciales
        /// </summary>
        /// <param name="value"></param>
        public HttpResponseMessage Post([FromBody]Dictionary<string, object> Credentials)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); }
            var status = _sesionAction.AuthenticateUser(Credentials);
            return status.CreateHttpResponse(Request);
        }

        // PUT: api/Sessions/5
        /// <summary>
        /// Actualizamos una session cuando hacemos el refresh
        /// de la misma para seguir conectados
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public HttpResponseMessage Put(string sessionId, [FromBody]string token)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); }
            var status = _sesionAction.ReconnectSession(token, sessionId);
            return status.CreateHttpResponse(Request);
        }

        // DELETE: api/Sessions/5
        /// <summary>
        /// Eliminamos una session cuando nos deslogeamos
        /// </summary>
        /// <param name="id"></param>
        public HttpResponseMessage Delete(string sessionId)
        {
            var status = _sesionAction.LeaveSession(sessionId);
            return status.CreateHttpResponse(Request);
        }
    }
}
