﻿using AxosnetAPI.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AxosnetAPI.Controllers
{
    public class AccountsController : ApiController
    {
        /// <summary>
        /// Clase para acceder a las acciones de Account
        /// </summary>
        private readonly AccountAction _accountAction;

        /// <summary>
        /// Constructor public preparado para DI
        /// </summary>
        public AccountsController()
        {
            _accountAction = new AccountAction();
        }

        // GET: api/Accounts
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Accounts/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Accounts
        /// <summary>
        /// Crea un nuevo usuario con contraseña para acceder al sistema
        /// </summary>
        /// <param name="accountInformation"></param>
        public HttpResponseMessage Post([FromBody]Dictionary<string,object> accountInformation)
        {
            if (!ModelState.IsValid) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); }
            var status = _accountAction.RegisterNewAccount(accountInformation);
            return status.CreateHttpResponse(Request);
        }

        // PUT: api/Accounts/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Accounts/5
        public void Delete(int id)
        {
        }
    }
}
