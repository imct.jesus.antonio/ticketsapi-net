﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AxosnetAPI.Entities;

namespace AxosnetAPI.DbAccess
{
    public class ProviderAccess
    {

        /// <summary>
        /// Carga todos los proveedores existentes en la base de datos
        /// </summary>
        /// <param name="_axos"></param>
        /// <returns></returns>
        public List<Provider> LoadAllProviders(AxosContext _axos)
        {
            if (_axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return _axos.Providers.Select(x => x).ToList();
        }

        /// <summary>
        /// Crea un nuevo proveedor en la base de datos que se pasa como segundo parametro
        /// y le asigna el nombre del primer parametro
        /// </summary>
        /// <param name="name"></param>
        /// <param name="_axos"></param>
        /// <returns></returns>
        public Provider CreateNewProvider(string name, AxosContext _axos)
        {
            if (_axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            var newProvider = new Provider(name);
            _axos.Providers.Add(newProvider);
            return newProvider;
        }
    }
}