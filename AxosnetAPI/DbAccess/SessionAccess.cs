﻿using AxosnetAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.DbAccess
{
    public class SessionAccess
    {

        /// <summary>
        /// Carga una session desde la base de datos con el sessionid que coincide
        /// con el que se pasa como parametro, junto con su cuenta relaccionada
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="_axos"></param>
        /// <returns></returns>
        public Session LoadSessionWithSessionIdIncludeAccount(string sessionId,AxosContext _axos)
        {
            if (_axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return _axos.Sessions
                        .Include("Account")
                        .Where(x => x.Refresh.Equals(sessionId))
                        .FirstOrDefault();
        }

        /// <summary>
        /// Carga una session desde la base de datos con el sessionid que se pasa como parametro
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="_axos"></param>
        /// <returns></returns>
        public Session LoadSessionWithSessionId(string sessionId, AxosContext _axos)
        {
            if (_axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return _axos.Sessions.Where(x => x.Refresh.Equals(sessionId)).FirstOrDefault();
        }
    }
}