﻿using AxosnetAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.DbAccess
{
    public class AccountAccess
    {
        /// <summary>
        /// Carga una cuenta que tenga el username que
        /// se pasa como parametro
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Account LoadAccountFullWithUsername(string username,AxosContext _axos)
        {
            if (_axos == null){ throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            // Cargamos la cuenta que coincida
            return _axos.Accounts
                .Include("Access")
                .Include("Personal")
                .Include("Sessions")
                .Where(acc => acc.Access.Username.Equals(username))
                .FirstOrDefault();
        }

        /// <summary>
        /// Crea una nueva cuenta con todos sus agregados completos
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <param name="secondLastname"></param>
        /// <param name="birthday"></param>
        /// <param name="gender"></param>
        /// <param name="_axos"></param>
        /// <returns></returns>
        public Account CreateNewAccount(string username, string password, string name, string lastname, string secondLastname, DateTime birthday, string gender, AxosContext _axos)
        {
            // Si no hay contexto
            if (_axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            // Creamos la cuenta
            var newAccount = new Account(username, password, name, lastname, secondLastname, birthday, gender);
            _axos.Accounts.Add(newAccount);
            // Retornamos la cuenta 
            return newAccount;
        }
    }
}