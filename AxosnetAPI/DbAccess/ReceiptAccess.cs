﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AxosnetAPI.Entities;

namespace AxosnetAPI.DbAccess
{
    public class ReceiptAccess
    {
        /// <summary>
        /// Se encarga de crear una instancia de recibo y agregarla a la base de datos
        /// que se pasa como ultimo parametro
        /// </summary>
        /// <param name="providerID"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="comments"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public Receipt CreateNewReceipt(int providerID, decimal amount, string currency, string comments, AxosContext axos)
        {
            if (axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            // Creamos el recibo
            var newReceipt = new Receipt(providerID, amount, currency, comments);
            axos.Receipts.Add(newReceipt);
            return newReceipt;
        }

        /// <summary>
        /// Carga un recibo con el id que se pasa como parametro
        /// </summary>
        /// <param name="receiptId"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public Receipt LoadReceiptWithId(int receiptId, AxosContext axos)
        {
            if (axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return axos.Receipts.Find(receiptId);
        }

        /// <summary>
        /// Carga un recibo con los detalles y el proveedor relacionado
        /// que coincida con el Id que se especifica por parametro
        /// </summary>
        /// <param name="receiptId"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public Receipt LoadReceiptIncludingDetailAndProviderWhithId(int receiptId, AxosContext axos)
        {
            if (axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return axos.Receipts
                        .Include("Detail")
                        .Include("Provider")
                        .Where(x => x.ReceiptID == receiptId && !x.IsDeleted)
                        .FirstOrDefault();
        }

        /// <summary>
        /// Carga los recibos con los detalles y el proveedor cuando
        /// coincidan con el proveedor id yq eu tengan el tipo de moneda que se pasa
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="currency"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public List<Receipt> LoadReceiptIncludingDetailAndProviderWithFilters(int providerId, string currency, AxosContext axos)
        {
            if (axos == null) { throw new InvalidOperationException("No se puede acceder a la base de datos"); }
            return axos.Receipts
                .Include("Detail")
                .Include("Provider")
                .Where(x => !x.IsDeleted
                && x.ProviderID == (providerId != 0 ? providerId : x.ProviderID)
                && x.Detail.Currency == (string.IsNullOrEmpty(currency) ? x.Detail.Currency : currency))
                .Select(x => x)
                .ToList();
        }
    }
}