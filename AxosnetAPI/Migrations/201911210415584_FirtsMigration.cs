﻿namespace AxosnetAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirtsMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Detail",
                c => new
                    {
                        DetailID = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(nullable: false),
                        LastUpdate = c.DateTime(nullable: false),
                        Comments = c.String(),
                        ReceiptID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ReceiptID)
                .ForeignKey("dbo.Receipt", t => t.ReceiptID)
                .Index(t => t.ReceiptID);
            
            CreateTable(
                "dbo.Receipt",
                c => new
                    {
                        ReceiptID = c.Int(nullable: false, identity: true),
                        ProviderID = c.Int(nullable: false),
                        DetailID = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ReceiptID)
                .ForeignKey("dbo.Provider", t => t.ProviderID, cascadeDelete: true)
                .Index(t => t.ProviderID);
            
            CreateTable(
                "dbo.Provider",
                c => new
                    {
                        ProviderID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProviderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Receipt", "ProviderID", "dbo.Provider");
            DropForeignKey("dbo.Detail", "ReceiptID", "dbo.Receipt");
            DropIndex("dbo.Receipt", new[] { "ProviderID" });
            DropIndex("dbo.Detail", new[] { "ReceiptID" });
            DropTable("dbo.Provider");
            DropTable("dbo.Receipt");
            DropTable("dbo.Detail");
        }
    }
}
