﻿namespace AxosnetAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Session",
                c => new
                    {
                        SessionID = c.Int(nullable: false, identity: true),
                        AccountID = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        Origin = c.String(nullable: false),
                        Refresh = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SessionID)
                .ForeignKey("dbo.Account", t => t.AccountID, cascadeDelete: true)
                .Index(t => t.AccountID);
            
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        PersonalID = c.Int(nullable: false),
                        AccessID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID);
            
            CreateTable(
                "dbo.Access",
                c => new
                    {
                        AccountID = c.Int(nullable: false),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.Account", t => t.AccountID)
                .Index(t => t.AccountID);
            
            CreateTable(
                "dbo.Personal",
                c => new
                    {
                        AccountID = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        SecondLastName = c.String(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                        Gender = c.String(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AccountID)
                .ForeignKey("dbo.Account", t => t.AccountID)
                .Index(t => t.AccountID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Session", "AccountID", "dbo.Account");
            DropForeignKey("dbo.Personal", "AccountID", "dbo.Account");
            DropForeignKey("dbo.Access", "AccountID", "dbo.Account");
            DropIndex("dbo.Personal", new[] { "AccountID" });
            DropIndex("dbo.Access", new[] { "AccountID" });
            DropIndex("dbo.Session", new[] { "AccountID" });
            DropTable("dbo.Personal");
            DropTable("dbo.Access");
            DropTable("dbo.Account");
            DropTable("dbo.Session");
        }
    }
}
