﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    /// <summary>
    /// Tabla de proveedor
    /// </summary>
    public class Provider
    {
        // Propiedades de tabla
        public int ProviderID { get; set; }
        public string Name { get; set; }

        // Propiedad de navegacion
        public virtual ICollection<Receipt> Receipts { get; set; }

        /// <summary>
        /// Constructor para crear un proveedor valido
        /// </summary>
        /// <param name="providerName"></param>
        public Provider(string providerName)
        {
            Receipts = new HashSet<Receipt>();
            if (string.IsNullOrEmpty(providerName))
            {
                throw new ArgumentNullException(nameof(providerName), "El nombre de proveedor no puede ser nulo");
            }
            Name = providerName;
        }

        /// <summary>
        /// Constructor EF
        /// </summary>
        private Provider()
        {
            Receipts = new HashSet<Receipt>();
        }
    }
}