﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    public class AxosContext : DbContext
    {
        public AxosContext() : base("AxosContext")
        {

        }

        // Listas de objetos
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Detail> Details { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Access> Accesses { get; set; }
        public DbSet<Personal> Personals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Configuracion de llaves primarias
            modelBuilder.Entity<Receipt>().HasKey(r => r.ReceiptID);
            modelBuilder.Entity<Detail>().HasKey(d => d.ReceiptID);
            modelBuilder.Entity<Provider>().HasKey(p => p.ProviderID);
            modelBuilder.Entity<Session>().HasKey(s => s.SessionID);
            modelBuilder.Entity<Account>().HasKey(a => a.AccountID);
            modelBuilder.Entity<Access>().HasKey(a => a.AccountID);
            modelBuilder.Entity<Personal>().HasKey(p => p.AccountID);



            // Configurando columnas
            // Receipt
            modelBuilder.Entity<Receipt>()
                .Property(p => p.ReceiptID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Receipt>()
                .Property(p => p.ProviderID)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Receipt>()
                .Property(p => p.DetailID)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Receipt>()
                .Property(p => p.CreateDate)
                .HasColumnOrder(4)
                .IsRequired();

            modelBuilder.Entity<Receipt>()
                .Property(p => p.IsDeleted)
                .HasColumnOrder(5)
                .IsRequired();

            // Detail
            modelBuilder.Entity<Detail>()
                .Property(d => d.DetailID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Detail>()
                .Property(d => d.Amount)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Detail>()
                .Property(d => d.Currency)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Detail>()
                .Property(d => d.LastUpdate)
                .HasColumnOrder(4)
                .IsRequired();

            modelBuilder.Entity<Detail>()
                .Property(d => d.Comments)
                .HasColumnOrder(5)
                .IsOptional();

            // Provider
            modelBuilder.Entity<Provider>()
                .Property(p => p.ProviderID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Provider>()
                .Property(p => p.Name)
                .HasColumnOrder(2)
                .IsRequired();

            // Account
            modelBuilder.Entity<Account>()
                .Property(a => a.AccountID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.CreationDate)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.PersonalID)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Account>()
                .Property(a => a.AccessID)
                .HasColumnOrder(4)
                .IsRequired();

            // Access
            modelBuilder.Entity<Session>()
                .Property(s => s.SessionID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Session>()
                .Property(s => s.AccountID)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Session>()
                .Property(s => s.CreationDate)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Session>()
                .Property(s => s.Origin)
                .HasColumnOrder(4)
                .IsRequired();

            modelBuilder.Entity<Session>()
                .Property(s => s.Refresh)
                .HasColumnOrder(5)
                .IsRequired();

            // Access
            modelBuilder.Entity<Access>()
                .Property(a => a.AccountID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Access>()
                .Property(a => a.Username)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Access>()
                .Property(a => a.Password)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Access>()
                .Property(a => a.UpdateDate)
                .HasColumnOrder(4)
                .IsRequired();

            // Personal
            modelBuilder.Entity<Personal>()
                .Property(p => p.AccountID)
                .HasColumnOrder(1)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.Name)
                .HasColumnOrder(2)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.LastName)
                .HasColumnOrder(3)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.SecondLastName)
                .HasColumnOrder(4)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.Birthday)
                .HasColumnOrder(5)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.Gender)
                .HasColumnOrder(6)
                .IsRequired();

            modelBuilder.Entity<Personal>()
                .Property(p => p.UpdateDate)
                .HasColumnOrder(7)
                .IsRequired();

            //  Relaciones
            // receipt => provider one to many
            modelBuilder.Entity<Receipt>()
                .HasRequired(p => p.Provider)
                .WithMany(p => p.Receipts)
                .HasForeignKey(p => p.ProviderID);

            // receipt => detail one to one
            modelBuilder.Entity<Receipt>()
                .HasRequired(r => r.Detail)
                .WithRequiredPrincipal(r => r.Receipt);

            // Access => Account onte to one 
            modelBuilder.Entity<Account>()
                .HasRequired(a => a.Access)
                .WithRequiredPrincipal(r => r.Account);

            // Personal => Account onte to one
            modelBuilder.Entity<Account>()
                .HasRequired(a => a.Personal)
                .WithRequiredPrincipal(a => a.Account);

            // Session => Account one to many
            modelBuilder.Entity<Session>()
                .HasRequired(a => a.Account)
                .WithMany(s => s.Sessions)
                .HasForeignKey(s => s.AccountID);
            


            //base.OnModelCreating(modelBuilder);
        }
    }
}