﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    /// <summary>
    /// Almacena las credenciales de la cuenta
    /// </summary>
    public class Access
    {
        /// <summary>
        /// Datos de tabla 
        /// </summary>
        public int AccountID { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Datos de navegacion
        /// </summary>
        public virtual Account Account { get; private set; }

        /// <summary>
        /// Constructor de EF
        /// </summary>
        private Access() { }

        /// <summary>
        /// Constructor de objeto
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public Access(string username, string password)
        {
            Validate(username, password);
            Username = username;
            Password = password;
            UpdateDate = DateTime.Now;
        }

        /// <summary>
        /// Validacion de campos
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        private void Validate(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("El username no puede ser nulo", nameof(username));
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("El password no puede ser nulo", nameof(password));
            }
        }
    }
}