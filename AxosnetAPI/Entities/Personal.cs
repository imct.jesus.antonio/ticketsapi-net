﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    /// <summary>
    /// Clase que contiene la informacion personal de la cuenta que se
    /// crea
    /// </summary>
    public class Personal
    {
        /// <summary>
        /// Datos de tabla
        /// </summary>
        public int AccountID { get; private set; }
        public string Name { get; private set; }
        public string LastName { get; private set; }
        public string SecondLastName { get; private set; }
        public DateTime Birthday { get; private set; }
        public string Gender { get; private set; }
        public DateTime UpdateDate { get; private set; }

        /// <summary>
        /// Datos de navegacion
        /// </summary>
        public virtual Account Account { get; private set; }

        /// <summary>
        /// Constructor de EF
        /// </summary>
        private Personal() { }

        /// <summary>
        /// Constructor para crear el objeto
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <param name="secondLastname"></param>
        /// <param name="birthday"></param>
        /// <param name="gender"></param>
        public Personal(string name, string lastname, string secondLastname, DateTime birthday, string gender)
        {
            // Validacion
            Validate(name, lastname, secondLastname, birthday, gender);
            // Asignacion
            Name = name;
            LastName = lastname;
            SecondLastName = secondLastname;
            Birthday = birthday;
            Gender = gender;
            UpdateDate = DateTime.Now;
        }

        /// <summary>
        /// Validamos que los datos que recibimos sean los correctos
        /// </summary>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <param name="secondLastname"></param>
        /// <param name="birthday"></param>
        /// <param name="gender"></param>
        private void Validate(string name, string lastname, string secondLastname, DateTime birthday, string gender)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("El nombre no puee ser vacio o nulo", nameof(name));
            }
            if (string.IsNullOrEmpty(lastname))
            {
                throw new ArgumentException("El primer apellido no puee ser vacio o nulo", nameof(lastname));
            }
            if (string.IsNullOrEmpty(secondLastname))
            {
                throw new ArgumentException("El segundo apellido no puee ser vacio o nulo", nameof(secondLastname));
            }
            if (birthday.Equals(new DateTime()))
            {
                throw new ArgumentException("La fecha de nacimiento no puee ser vacio o nulo", nameof(birthday));
            }
            if (string.IsNullOrEmpty(gender))
            {
                throw new ArgumentException("El genero no puee ser vacio o nulo", nameof(gender));
            }
        }
    }
}