﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    public class Detail
    {
        // Propiedades de tabla
        public int DetailID { get; private set; }
        public int ReceiptID { get; private set; }
        public decimal Amount { get; private set; }
        public string Currency { get; private set; }
        public DateTime LastUpdate { get; private set; }
        public string Comments { get; private set; }

        // Propiedades de navegacion
        public virtual Receipt Receipt { get; private set; }

        /// <summary>
        /// Constructor del objeto detalle
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="comments"></param>
        public Detail(decimal amount, string currency, string comments)
        {
            // Validaciones
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), amount, "El monto no puede ser menor a cero");
            }
            if (string.IsNullOrEmpty(currency))
            {
                throw new ArgumentNullException(nameof(currency), "El tipo de moneda no puede ser nulo");
            }

            // Seteo de variables
            Amount = amount;
            Currency = currency;
            LastUpdate = DateTime.Now;
            Comments = comments is null ? string.Empty : comments;
        }

        /// <summary>
        /// Actualiza el monto de el detalle cargado
        /// </summary>
        /// <param name="newAmount"></param>
        /// <returns></returns>
        internal bool UpdateAmount(decimal newAmount)
        {
            if (newAmount < 0) { return false; }
            Amount = newAmount;
            RegisterUpdate();
            return true;
        }

        /// <summary>
        /// Axtualiza la moneda en la que se representa el recibo
        /// </summary>
        /// <param name="newCurrency"></param>
        /// <returns></returns>
        internal bool UpdateCurrency(string newCurrency)
        {
            if (string.IsNullOrEmpty(newCurrency)) { return false; }
            Currency = newCurrency;
            RegisterUpdate();
            return true;
        }

        /// <summary>
        /// Actualiza los comentarios del detalle
        /// </summary>
        /// <param name="newComments"></param>
        /// <returns></returns>
        internal bool UpdateComments(string newComments)
        {
            if (string.IsNullOrEmpty(newComments)) { return false; }
            Comments = newComments;
            RegisterUpdate();
            return true;
        }

        /// <summary>
        /// Actualiza la fecha que se modifico el registro
        /// </summary>
        private void RegisterUpdate()
        {
            LastUpdate = DateTime.Now;
        }

        /// <summary>
        /// Constructor vacio para EF
        /// </summary>
        private Detail() { }
    }
}