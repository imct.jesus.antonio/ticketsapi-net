﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    /// <summary>
    /// Tabla de recibos
    /// </summary>
    public class Receipt
    {
        // Campos de la tabla
        public int ReceiptID { get; private set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; private set; }
        public int ProviderID { get; private set; }
        public int DetailID { get; private set; }

        // Propiedades de navegacion
        public virtual Provider Provider { get; private set; }
        public virtual Detail Detail { get; private set; }

        /// <summary>
        /// Constructor con proveedor asegurado
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="amount"></param>
        /// <param name="currency"></param>
        /// <param name="comments"></param>
        public Receipt(int providerId, decimal amount, string currency,string comments)
        {
            // Validacion
            if (providerId < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(providerId),providerId,"El proveedor no es valido");
            }

            // Se crea, se valida y se asigna
            Detail = new Detail(amount, currency, comments);

            // Seteo
            this.CreateDate = DateTime.Now;
            this.IsDeleted = false;
            this.ProviderID = providerId;
        }

        /// <summary>
        /// Constructor para EF
        /// </summary>
        private Receipt()
        {

        }

        /// <summary>
        /// Cambia el proveedor asignado al recibo
        /// </summary>
        /// <param name="NewProviderId"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public bool ChangeProvider(int NewProviderId,AxosContext axos)
        {
            Provider newProvider = axos.Providers.Find(NewProviderId);
            if (newProvider is null)
            {
                return false;
            }
            this.Provider = newProvider;
            return true;
        }

        /// <summary>
        /// Manda a modificar el monto del detalle
        /// </summary>
        /// <param name="newAmount"></param>
        /// <returns></returns>
        public bool ChangeAmount(decimal newAmount)
        {
            if (Detail is null){ return false; }
            return Detail.UpdateAmount(newAmount);
        }

        /// <summary>
        /// Manda al objeto de detalle que actualice la moneda
        /// </summary>
        /// <param name="newCurrency"></param>
        /// <returns></returns>
        public bool ChangeCurrency(string newCurrency)
        {
            if (Detail is null) { return false; }
            return Detail.UpdateCurrency(newCurrency);
        }

        /// <summary>
        /// Ordena al objeto que actualice los comentarios
        /// </summary>
        /// <param name="newComments"></param>
        /// <returns></returns>
        public bool UpdateComments(string newComments)
        {
            if (Detail is null) { return false; }
            return Detail.UpdateComments(newComments);
        }

        /// <summary>
        /// Marca el registro actual como eliminado
        /// </summary>
        /// <returns></returns>
        public bool LogicalDeleted()
        {
            IsDeleted = true;
            return true;
        }
    }
}