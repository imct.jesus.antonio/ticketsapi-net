﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    public class Account
    {
        /// <summary>
        /// Datos de tabla
        /// </summary>
        public int AccountID { get; private set; }
        public DateTime CreationDate { get; private set; }
        public int PersonalID { get; private set; }
        public int AccessID { get; private set; }

        /// <summary>
        /// Datos de navegacion 
        /// </summary>
        public virtual Personal Personal { get; private set; }
        public virtual Access Access { get; private set; }
        public virtual ICollection<Session> Sessions { get; private set; }

        /// <summary>
        /// Constructor para EF
        /// </summary>
        private Account()
        {
            Sessions = new HashSet<Session>();
        }

        /// <summary>
        /// Constructor para el objeto completo
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <param name="lastname"></param>
        /// <param name="secondLastname"></param>
        /// <param name="birthday"></param>
        /// <param name="gender"></param>
        public Account(string username, string password,string name, string lastname, string secondLastname, DateTime birthday, string gender)
        {
            // Creamos el personal
            Personal = new Personal(name,lastname,secondLastname,birthday,gender);
            // Creamos las credenciales
            Access = new Access(username,password);
            // Actualizamos la fecha de creacion
            CreationDate = DateTime.Now;
        }

        /// <summary>
        /// Refresca el token de session de la 
        /// session que coincida con ese id
        /// </summary>
        /// <param name="id"></param>
        public string ReuseSession(string origin, AxosContext axos = null)
        {
            if (Sessions != null)
            {
                // Reutilizamos
                Session SessionToReuse = Sessions.Where(x => x.Origin.Equals(origin)).FirstOrDefault();
                return SessionToReuse.UpdateSessionId();
            }
            else if (axos != null)
            {
                // reutilizamos
                Session SessionToReuse = axos.Sessions
                    .Where(x => x.AccountID == AccountID && x.Origin.Equals(origin))
                    .FirstOrDefault();
                return SessionToReuse.UpdateSessionId();
            }
            else
            {
                // excepcion
                throw new ArgumentNullException(nameof(axos), "No se pudo reutilizar la session");
            }
        }

        /// <summary>
        /// Genera una relacion con una sesion nueva para indicar que
        /// se ha asignado un id para refrescar token
        /// </summary>
        /// <param name="origin"></param>
        /// <returns></returns>
        public string GenerateSession(string origin, AxosContext axos = null)
        {
            string sessionId = Guid.NewGuid().ToString();
            if (Sessions != null)
            {
                // Reutilizamos
                Sessions.Add(new Session(origin, sessionId));
            }
            else if (axos != null)
            {
                // reutilizamos
                axos.Sessions.Add(new Session(origin, sessionId));
            }
            else
            {
                // excepcion
                throw new ArgumentNullException(nameof(axos), "No se pudo agregar la session");
            }
            return sessionId;
        }

        /// <summary>
        /// Busca una sesion en el contexto
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="axos"></param>
        /// <returns></returns>
        public bool FindSessionWithOrigin(string origin,AxosContext axos = null)
        {
            if (Sessions != null)
            {
                // Buscamos la session y retornamos el estado
                return Sessions.Any(x => x.Origin.Equals(origin));
            }
            else if (axos != null)
            {
                return axos.Sessions.Any(x => x.AccountID == AccountID && x.Origin.Equals(origin));
            }
            else
            {
                throw new ArgumentNullException(nameof(axos), "No hay forma de buscar una sesion");
            }
        }
    }
}