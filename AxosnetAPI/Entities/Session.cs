﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AxosnetAPI.Entities
{
    public class Session
    {
        /// <summary>
        /// Campos de tabla
        /// </summary>
        public int SessionID { get; private set; }
        public int AccountID { get; private set; }
        public DateTime CreationDate { get; private set; }
        public string Origin { get; private set; }
        public string Refresh { get; private set; }

        /// <summary>
        /// Datos de navegacion
        /// </summary>
        public virtual Account Account { get; private set; }

        /// <summary>
        /// Constructor de EF
        /// </summary>
        private Session() { }

        /// <summary>
        /// Constructor del objeto normal
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="refresh"></param>
        public Session(string origin, string refresh)
        {
            // Validacion
            Validate(origin, refresh);
            // Asignacion
            Origin = origin;
            Refresh = refresh;
            CreationDate = DateTime.Now;
        }

        /// <summary>
        /// Valida los datos para poder crear una sesion
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="refresh"></param>
        private void Validate(string origin, string refresh)
        {
            if (string.IsNullOrEmpty(origin))
            {
                throw new ArgumentException("El origen no puede ser nulo", nameof(origin));
            }
            if (string.IsNullOrEmpty(refresh))
            {
                throw new ArgumentException("El origen no puede ser nulo", nameof(refresh));
            }
        }

        /// <summary>
        /// Actualiza el id de la sesion para reutilizar
        /// el registro
        /// </summary>
        public string UpdateSessionId()
        {
            Refresh = Guid.NewGuid().ToString();
            return Refresh;
        }
    }
}