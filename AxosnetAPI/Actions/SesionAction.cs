﻿using AxosnetAPI.Entities;
using AxosnetAPI.DbAccess;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AxosnetAPI.Action
{
    public class SesionAction
    {
        /// <summary>
        /// Acceso a la base de datos
        /// </summary>
        private AxosContext _axos;
        private AccountAccess _accountAccess;
        private SessionAccess _sessionAccess;
        private TokenService _tokenService;
        private EncryptionService _encryptionService;

        public SesionAction()
        {
            _accountAccess = new AccountAccess();
            _sessionAccess = new SessionAccess();
            _encryptionService = new EncryptionService();
            _tokenService = new TokenService();
        }

        /// <summary>
        /// Proceso de negocio que permite a un usuario iniciar sesion
        /// en el sistema
        /// </summary>
        /// <param name="Credentials"></param>
        /// <returns></returns>
        public Result AuthenticateUser(Dictionary<string,object> Credentials)
        {
            try
            {
                // contenedor
                object data = new object();
                string origin = string.Empty;
                string username = string.Empty;
                string password = string.Empty;
                string sessionToken = string.Empty;
                string accessToken = string.Empty;

                // Si no se encuentra el valor lanzamos excepcion
                if (!Credentials.TryGetValue("origin", out data)){ throw new ArgumentNullException("El origen no puede ser nulo"); }
                origin = Convert.ToString(data);
                if (!Credentials.TryGetValue("username", out data)){ throw new ArgumentNullException("El username no puede ser nulo"); }
                username = Convert.ToString(data);
                if (!Credentials.TryGetValue("password", out data)){ throw new ArgumentNullException("El password no puede ser nulo"); }
                password = Convert.ToString(data);

                // Invocamos el contexto
                using (_axos = new AxosContext())
                {
                    Account matchingAccount = _accountAccess.LoadAccountFullWithUsername(username,_axos);
                    if (matchingAccount == null) { throw new KeyNotFoundException("Usuario no encontrado"); }
                    string decryptedPassword = _encryptionService.DecryptEncryptedString(matchingAccount.Access.Password);
                    if (string.IsNullOrEmpty(decryptedPassword) || !decryptedPassword.Equals(password))
                    {
                        throw new ArgumentException("Password incorrecto");
                    }
                    if (matchingAccount.FindSessionWithOrigin(origin)){ sessionToken = matchingAccount.ReuseSession(origin); }
                    else{ sessionToken = matchingAccount.GenerateSession(origin); }
                    _axos.SaveChanges();
                    accessToken = _tokenService.GenerateToken(matchingAccount);
                    return new Result("Autenticacion correcta",new{
                        Token = accessToken,
                        SessionId = sessionToken,
                        userId = matchingAccount.AccountID,
                        username = matchingAccount.Personal.Name
                    },HttpStatusCode.Created);
                }
            }
            catch (ArgumentNullException arg)
            {
                return new Result(arg, HttpStatusCode.BadRequest);
            }
            catch (KeyNotFoundException arg)
            {
                return new Result(arg, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Realiza la reconexion de la session si el tiempo
        /// de expiracion del token se encientra dentro de los 
        /// limites permitidos
        /// </summary>
        /// <param name="InfoSession"></param>
        /// <returns></returns>
        public Result ReconnectSession(string token, string sessionId)
        {
            try
            {
                // Obtenemos datos
                // Si no se encuentra el valor lanzamos excepcion
                if (string.IsNullOrEmpty(token)) { throw new ArgumentNullException("El token no puede ser nulo"); }
                if (string.IsNullOrEmpty(sessionId)) { throw new ArgumentNullException("El sessionId no puede ser nulo"); }
                if (!TokenService.HasValidLifeTime(token)) { throw new InvalidOperationException("El token tiene mas tiempo caducado que el permitido"); }
                // Invocamos la base de datos
                using (_axos = new AxosContext())
                {
                    Session sessionToRefresh = _sessionAccess.LoadSessionWithSessionIdIncludeAccount(sessionId,_axos);
                    if (sessionToRefresh == null) { throw new ArgumentException("El token tiene mas tiempo caducado que el permitido"); }
                    sessionId = sessionToRefresh.UpdateSessionId();
                    _axos.SaveChanges();
                    token = _tokenService.GenerateToken(sessionToRefresh.Account);
                    return new Result("Reconexion exitosa", new { Token = token, SessionId = sessionId }, HttpStatusCode.Created);
                }
            }
            catch (ArgumentException arg)
            {
                return new Result(arg, HttpStatusCode.NotFound);
            }
            catch (InvalidOperationException inv)
            {
                return new Result(inv, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Elimina una session de la base de datos
        /// que no podra ser encontrada de nuevo
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public Result LeaveSession(string sessionId)
        {
            try
            {
                // Validacion
                if (string.IsNullOrEmpty(sessionId)) { throw new ArgumentNullException("Ninguna session que eliminar"); }
                // Invocamos la base de datos
                using (_axos = new AxosContext())
                {
                    // Buscamos la session con ese id
                    Session sessionToLeave = _sessionAccess.LoadSessionWithSessionId(sessionId, _axos);
                    //Session sessionToLeave = _axos.Sessions.Where(x => x.Refresh.Equals(sessionId)).FirstOrDefault();
                    // si no existe
                    if (sessionToLeave == null) { throw new KeyNotFoundException("Ninguna session con el Id especificado"); }
                    // Eliminamos
                    _axos.Sessions.Remove(sessionToLeave);
                    // Salvamos los cambios
                    _axos.SaveChanges();
                    // retornamos resultado
                    return new Result("Session eliminada correctamente",HttpStatusCode.OK);
                }
            }
            catch (KeyNotFoundException ex)
            {
                return new Result(ex, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
        }
    }
}