﻿using AxosnetAPI.DbAccess;
using AxosnetAPI.Entities;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AxosnetAPI.Actions
{
    public class ReceiptAction
    {

        /// <summary>
        /// Contexto de la aplicacion
        /// </summary>
        private AxosContext _axos;

        /// <summary>
        /// Acceso a todos los recibos, habilita las consultas
        /// y las creaciones de los mismos
        /// </summary>
        private ReceiptAccess _receiptAccess;

        public ReceiptAction()
        {
            _receiptAccess = new ReceiptAccess();
        }

        /// <summary>
        /// Cumple la regla de negocio para agregar un nuevo recibo
        /// </summary>
        /// <param name="ReceiptData"></param>
        /// <returns></returns>
        public Result AddNewReceipt(Dictionary<string, object> ReceiptData)
        {
            // Validamos que los datos vengan completos
            object data = new object();
            int providerID = 0;
            decimal amount = 0;
            string currency = string.Empty;
            string comments = string.Empty;

            // Intentamos obtener los datos
            try
            {
                providerID = ReceiptData.TryGetValue("providerID", out data) ? Convert.ToInt32(data) : 0;
                amount = ReceiptData.TryGetValue("amount", out data) ? Convert.ToDecimal(data) : 0;
                currency = ReceiptData.TryGetValue("currency", out data) ? Convert.ToString(data) : string.Empty;
                comments = ReceiptData.TryGetValue("comments", out data) ? Convert.ToString(data) : string.Empty;

                Receipt newReceipt;
                // Inicio de la accion con base de datos
                using (_axos = new AxosContext())
                {
                    newReceipt = _receiptAccess.CreateNewReceipt(providerID, amount, currency, comments,_axos);
                    _axos.SaveChanges();
                }
                // Salida
                return new Result("Recurso creado correctamente",
                    new
                    {
                        newReceipt.ReceiptID,
                        newReceipt.Detail.Amount,
                        newReceipt.Detail.Comments,
                        newReceipt.Detail.Currency,
                        newReceipt.Detail.LastUpdate,
                        newReceipt.ProviderID
                    }, HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Cumple la regla de negocio para modificar un proveedor
        /// </summary>
        /// <param name="ReceiptToUpdate"></param>
        /// <param name="newProviderId"></param>
        /// <returns></returns>
        public Result ChangeProvider(Dictionary<string, object> ReceiptData)
        {
            // Validamos que los datos vengan completos
            object data = new object();
            int providerID = 0;
            int receiptId = 0;

            // Intentamos obtener los datos
            try
            {
                providerID = ReceiptData.TryGetValue("providerID", out data) ? 0 : Convert.ToInt32(data);
                receiptId = ReceiptData.TryGetValue("receiptId", out data) ? 0 : Convert.ToInt32(data);

                Receipt receipt;
                // Inicio de la accion con base de datos
                using (_axos = new AxosContext())
                {
                    receipt = _receiptAccess.LoadReceiptWithId(receiptId,_axos);
                    if (receipt is null) { throw new ArgumentNullException(nameof(receiptId), "El id del recibo no existe"); }
                    receipt.ChangeProvider(providerID, _axos);
                    _axos.SaveChanges();
                }

                return new Result("Recurso actualizado correctamente", receipt, HttpStatusCode.OK);
            }
            catch (ArgumentNullException ex)
            {
                return new Result(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Cumple la regla de negocio para actualizar los detalles del recibo
        /// </summary>
        /// <param name="UpdateData"></param>
        /// <returns></returns>
        public Result UpdateDeferredDetail(int receiptId, Dictionary<string, object> UpdateData)
        {
            try
            {
                // Objetos de entrada
                object data = new object();
                //int receiptId = 0;
                decimal amount = 0;
                string currency = string.Empty;
                string comments = string.Empty;
                int providerId = 0;
                Receipt ChargedReceipt;

                // Obtencion de valores
                providerId = UpdateData.TryGetValue("providerId", out data) ? Convert.ToInt32(data) : 0;
                amount = UpdateData.TryGetValue("amount", out data) ? Convert.ToInt32(data) : 0;
                currency = UpdateData.TryGetValue("currency", out data) ? Convert.ToString(data) : string.Empty;
                comments = UpdateData.TryGetValue("comments", out data) ? Convert.ToString(data) : string.Empty;

                // Carga de contexto
                using (_axos = new AxosContext())
                {
                    // Cargamos el recibo
                    ChargedReceipt = _receiptAccess.LoadReceiptIncludingDetailAndProviderWhithId(receiptId,_axos);
                    if (ChargedReceipt is null) { throw new ArgumentNullException(nameof(receiptId), "El id del recibo no existe"); }
                    // Verificamos que cambiamos
                    ChargedReceipt.ChangeAmount(amount);
                    ChargedReceipt.ChangeCurrency(currency);
                    ChargedReceipt.UpdateComments(comments);
                    ChargedReceipt.ChangeProvider(providerId, _axos);
                    // salvamos
                    _axos.SaveChanges();
                    // Retornamos
                    return new Result("Recurso actualizado correctamente", HttpStatusCode.OK);
                }
            }
            catch (ArgumentNullException ex)
            {
                return new Result(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Cumple la regla de negocio para eliminar logicamente un registro
        /// </summary>
        /// <param name="ReceiptIdToDelete"></param>
        /// <returns></returns>
        public Result LogicalDeletionOfReceipt(int ReceiptIdToDeleteId)
        {
            try
            {
                // Iniciamos la base de datos
                using (_axos = new AxosContext())
                {
                    // Cargamos el registro
                    Receipt receiptToDelete = _receiptAccess.LoadReceiptWithId(ReceiptIdToDeleteId,_axos);
                    if (receiptToDelete is null || receiptToDelete.IsDeleted) { throw new ArgumentNullException(nameof(receiptToDelete), "El id del recibo no existe"); }
                    receiptToDelete.LogicalDeleted();
                    _axos.SaveChanges();
                    return new Result("Recurso eliminado correctamente", HttpStatusCode.OK);
                }
            }
            catch (ArgumentNullException ex)
            {
                return new Result(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Recibe filtros que limitan los recibos a cargar
        /// </summary>
        /// <param name="receiptFilters"></param>
        /// <returns></returns>
        public Result ReceiptLoadAll(int providerId, string currency)
        {
            try
            {
                List<Receipt> ReceiptCollection = new List<Receipt>();
                // cargamos la base de datos
                using (_axos = new AxosContext())
                {
                    ReceiptCollection = _receiptAccess.LoadReceiptIncludingDetailAndProviderWithFilters(providerId,currency,_axos);
                }

                var salida = from R in ReceiptCollection
                             select new
                             {
                                 id = R.ReceiptID,
                                 amount = R.Detail.Amount,
                                 comments = R.Detail.Comments,
                                 currency = R.Detail.Currency,
                                 providerId = R.Provider.ProviderID,
                                 provider = R.Provider.Name
                             };

                return new Result("Registros obtenidos correctamente", salida, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Carga un recibo que coincida con el id que se pasa como parametro
        /// </summary>
        /// <param name="receiptIdToLoad"></param>
        /// <returns></returns>
        public Result ReceiptSelect(int receiptIdToLoad)
        {
            try
            {
                Receipt receiptLoad;
                // Usamos la base de datos
                using (_axos = new AxosContext())
                {
                    receiptLoad = _receiptAccess.LoadReceiptIncludingDetailAndProviderWhithId(receiptIdToLoad,_axos);
                    if (receiptLoad is null) { throw new ArgumentNullException(nameof(receiptIdToLoad), "El id del recibo no existe"); }
                }
                return new Result("Registro cargado correctamente", new
                {
                    receiptLoad.ReceiptID,
                    receiptLoad.Detail.Amount,
                    receiptLoad.Detail.Comments,
                    receiptLoad.Detail.Currency,
                    receiptLoad.Provider.ProviderID,
                    receiptLoad.Provider.Name
                }, HttpStatusCode.OK);
            }
            catch (ArgumentNullException ex)
            {
                return new Result(ex.Message, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }
    }
}