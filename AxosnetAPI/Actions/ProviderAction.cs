﻿using AxosnetAPI.DbAccess;
using AxosnetAPI.Entities;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AxosnetAPI.Actions
{
    public class ProviderAction
    {
        /// <summary>
        /// Contexto de la base de datos
        /// </summary>
        private AxosContext _axos;
        private ProviderAccess _providerAccess;

        public ProviderAction()
        {
            _providerAccess = new ProviderAccess();
        }

        /// <summary>
        /// Carga a todos los proveedores utilizando la clase de acceso a la base de datos.
        /// Esta carga a los proveedores y la clase padre o retorna un error interno si
        /// fallase.
        /// </summary>
        /// <returns></returns>
        public Result ProviderLoadAll()
        {
            try
            {
                List<Provider> providerCollection = _providerAccess.LoadAllProviders(_axos);
                return new Result("Registros obtenidos correctamente", providerCollection, HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new Result(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Agrega un nuevo proveedor a la base de datos
        /// </summary>
        /// <param name="providerData"></param>
        /// <returns></returns>
        public Result AddNewProvider(Dictionary<string, object> providerData)
        {
            // Validamos que los datos vengan completos
            object data = new object();
            string name = string.Empty;

            try
            {
                name = providerData.TryGetValue("providerName", out data) ? Convert.ToString(data) : string.Empty;
                Provider newProvider;
                // Inicio de la accion con base de datos
                using (_axos = new AxosContext())
                {
                    newProvider = _providerAccess.CreateNewProvider(name,_axos);
                    _axos.SaveChanges();
                }
                return new Result("Recurso creado correctamente", newProvider, HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
        }
    }
}