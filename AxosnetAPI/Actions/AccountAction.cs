﻿using AxosnetAPI.DbAccess;
using AxosnetAPI.Entities;
using AxosnetAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AxosnetAPI.Actions
{
    public class AccountAction
    {
        /// <summary>
        /// Acceso a la base de datos
        /// </summary>
        private AxosContext _axos;
        private AccountAccess AccountAccess;
        private NotificationService NotificationService;
        private EncryptionService _encryptionService;

        // Datos estaticos para la encryptacion de contraseña

        public AccountAction()
        {
            AccountAccess = new AccountAccess();
            NotificationService = new NotificationService();
            _encryptionService = new EncryptionService();
        }

        /// <summary>
        /// Registra una nueva cuenta dentro de la base de datos
        /// y envia un correo para confirmar la cuenta
        /// </summary>
        /// <param name="userInformation"></param>
        /// <returns></returns>
        public Result RegisterNewAccount(Dictionary<string,object> userInformation)
        {
            try
            {
                // Variables para usuario
                object data = new object();
                string username = string.Empty;
                string password = string.Empty;
                string name = string.Empty;
                string lastname = string.Empty;
                string secondLastname = string.Empty;
                DateTime birthday = new DateTime();
                string gender = string.Empty;

                // Obteniendo los datos
                if (!userInformation.TryGetValue("username", out data)) { throw new ArgumentNullException("El username no puede ser nulo"); }
                username = Convert.ToString(data);
                if (!userInformation.TryGetValue("password", out data)) { throw new ArgumentNullException("El password no puede ser nulo"); }
                password = Convert.ToString(data);
                if (!userInformation.TryGetValue("name", out data)) { throw new ArgumentNullException("El name no puede ser nulo"); }
                name = Convert.ToString(data);
                if (!userInformation.TryGetValue("lastname", out data)) { throw new ArgumentNullException("El Lastname no puede ser nulo"); }
                lastname = Convert.ToString(data);
                if (!userInformation.TryGetValue("secondlastname", out data)) { throw new ArgumentNullException("El secodnLastname no puede ser nulo"); }
                secondLastname = Convert.ToString(data);
                if (!userInformation.TryGetValue("birthday", out data)) { throw new ArgumentNullException("El birthday no puede ser nulo"); }
                birthday = Convert.ToDateTime(data);
                if (!userInformation.TryGetValue("gender", out data)) { throw new ArgumentNullException("El gender no puede ser nulo"); }
                gender = Convert.ToString(data);
                // Invocamos la base de datos
                using (_axos = new AxosContext())
                {
                    // Encriptamos la contraseña
                    string encryptedPassword = _encryptionService.EncryptDecryptedString(password);
                    // Agregamos el registro
                    Account newAccount = AccountAccess.CreateNewAccount(username, encryptedPassword, name, lastname, secondLastname, birthday, gender,_axos);
                    // Salvamos la base de datos
                    _axos.SaveChanges();
                    // Enviamos correo de confirmacion
                    NotificationService.SendConfirmationAccountEmail(newAccount);
                    return new Result("Recurso creado correctamente",new
                    {
                        newAccount.AccountID,
                        newAccount.CreationDate,
                        newAccount.Personal.Name,
                        newAccount.Personal.LastName,
                        newAccount.Personal.SecondLastName,
                        newAccount.Personal.Gender,
                        newAccount.Personal.Birthday,
                        newAccount.Access.Username
                    } ,HttpStatusCode.Created);
                }
            }
            catch (Exception ex)
            {
                return new Result(ex, HttpStatusCode.BadRequest);
            }
            
        }

        /// <summary>
        /// Modifica la informacion personal de una cuenta
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="personalInformation"></param>
        /// <returns></returns>
        public Result ModifyPersonalInformation(int accountId, Dictionary<string, object> personalInformation)
        {
            return null;
        }

        /// <summary>
        /// Restablece la contraseña cuando el usuario olvida
        /// cual es
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Result RestorePassword(int accountId)
        {
            return null;
        }

        /// <summary>
        /// Permite modificar la contraseña de forma conciente al
        /// usuario
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public Result ChangePassword(int accountId,Dictionary<string,object> passwords)
        {
            return null;
        }
    }
}